Feature: Checks on cboevestfunds site

  Background:
    Given DEV separate server cboevestfunds.com is open

# last row from examples will fail in this scenario to demonstrate how this test catches bugs
# TODO adjust to support Ie
  @chrome @edge @firefox
  Scenario Outline: Ensure graph on Protection page is rebult when slider moves
    When Protection is selected
    Then Protection fund page is open
    # TBC if we need to pass default parameters into this check for default values
    And Protection widget is loaded with default values for stock -15% and strategy -5%
    And Screenshot is taken
    When Slider moves by "<horizontal_offset>" and "<vertical_offset>"
    Then Screenshot is taken
    And Graph is rebuilt with values "<changed_stock_value>" and "<changed_strategy_value>"
  Examples:
    | horizontal_offset  | vertical_offset  | changed_stock_value | changed_strategy_value |
    | 0                | -244               | 25%                 | 15%                    |
    | 0                | 92                 | -30%                | -20%                   |
  #  | 0                | -159               | -1%                 | -2%                    |


  @wip
  Scenario: Ensure axis labels on the graph show correct values
    When Protection is selected
    Then Protection fund page is open
    And Protection widget is loaded with all the axis labels resolved into 30%, 15%, -15% and -30%