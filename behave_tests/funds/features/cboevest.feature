Feature: Various navigation checks on cboevest.com

  Background:
    Given DEV cboevest.com is open

  @chrome @ie @firefox @edge
  Scenario: VestFit - Supplement Income -  Invest in ETF - Proceed to external site
    When 'Collect ongoing returns to supplement income' is selected
    And 'Investing in an ETF (exchange traded fund)' is selected
    And 'EXPLORE AN INCOME ETF' button is pressed
    Then ETF redirect popup window message appears
    When PROCEED is pressed on popup window
    Then ETF site is open

  @chrome @ie @firefox @edge
  Scenario: VestFit - Supplement Income -  Invest in ETF - Stayed on cboevest page
    When 'Collect ongoing returns to supplement income' is selected
    And 'Investing in an ETF (exchange traded fund)' is selected
    And 'EXPLORE AN INCOME ETF' button is pressed
    Then ETF redirect popup window message appears
    When 'Stay on this page' is clicked on popup window
    Then User stays on cboevest.com site

  @chrome @firefox @ie @edge
  Scenario: Ensure forwarding to Funds page works from headers menu
    When MUTUAL FUNDS is selected
    Then Mutual Funds redirect popup window message appears
    When PROCEED is pressed on popup window
    Then Mutual Funds page is open

  @chrome @firefox @ie @edge
  Scenario: Ensure forwarding to ETF page works from headers menu
    When ETFs is selected
    Then ETF redirect popup window message appears
    When PROCEED is pressed on popup window
    Then ETF site is open