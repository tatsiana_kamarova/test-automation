import time
from behave import *
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

from behave_tests.funds.commons import common_funds_methods as cfm
from behave_tests.commons import common_test_methods as ctm


@given('DEV cboevestfunds.com is open')
def go_to_dev_cboevestfunds_site(context):
    context.driver.get("https://www-dev.cboevestfunds.com")


@given('PROD cboevestfunds.com is open')
def go_to_prod_cboevestfunds_site(context):
    context.driver.get("https://cboevestfunds.com")


@given('DEV Buffer Protection site is open')
def go_to_dev_protection_site(context):
    context.driver.get("https://www-dev.cboevestfunds.com/protect-participate")


@when('Protection is selected')
def select_protection(context):
    context.driver.find_element_by_xpath('//a[@href="/protect-participate"]').click()


@then('Protection fund page is open')
def check_that_protection_funds_page_is_opened(context):
    cfm.check_that_expected_site_is_opened(context.driver, "protection", 10)


# TODO 'Widget is loaded with default values' keyword should contain table with input data and is yet to be implemented
# https://jenisys.github.io/behave.example/tutorials/tutorial06.html - reading of values table from keyword
@then('Widget is loaded with default values')
def check_default_widget_values(context):
    print('TODO')


# Implementation of the below is pretty lame: it waits for stock bar value to become -15% and after that checks if
#       strategy bar value is -5%. Ideally it should wait for slider to stop moving and then check both values for
#       strategy bar and stock bar
@then('Protection widget is loaded with default values for stock -15% and strategy -5%')
def check_default_widget_values(context):
    def wait_for_slider_to_stop(locator, expected_value):
        def check_condition(driver):
            bar_element = driver.find_element_by_css_selector(locator)
            bar_value = bar_element.text
            if bar_value == expected_value:
                return bar_element
            else:
                return False

        return check_condition

    # actions = ActionChains(context.driver)
    graph_section_selector = '#protection article'

    WebDriverWait(context.driver, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, graph_section_selector)))

    active_element = context.driver.find_element_by_css_selector(graph_section_selector)
    # actions.move_to_element(active_element).perform()

    context.driver.execute_script("arguments[0].scrollIntoView();", active_element)
    context.driver.switch_to.frame(context.driver.find_element_by_css_selector("#embedded-widget"))

    wait = WebDriverWait(context.driver, 10)
    wait_value = wait_for_slider_to_stop("#ipad-view div.stock.exmpl.column .text", "-15%")

    wait.until(wait_value)

    stock_bar_value = cfm.get_bar_value("#ipad-view div.stock.exmpl.column .text", context.driver)
    strategy_bar_value = cfm.get_bar_value("#ipad-view div.armor.exmpl.column .text", context.driver)
    print("!!! NOTES FOR DEBUGGING !!! Default Stock bar value: " + stock_bar_value
          + "\n!!! NOTES FOR DEBUGGING !!! Default Strategy bar value: " + strategy_bar_value)

    assert stock_bar_value == "-15%"
    assert strategy_bar_value == "-5%"


@when('Slider moves by "{horizontal_offset}" and "{vertical_offset}"')
def move_slider(context, horizontal_offset, vertical_offset):
    slider = context.driver.find_element_by_css_selector("#demo-slider .noUi-handle")
    # slider = context.driver.find_element_by_xpath('//*[@id="demo-slider"]/div/div/div')
    move = ActionChains(context.driver)

    print("!!! NOTES FOR DEBUGGING !!! Slider default location: " + str(slider.location))
    move.click_and_hold(slider).move_by_offset(horizontal_offset, vertical_offset).release().perform()
    # move.drag_and_drop_by_offset(slider, horizontal_offset, vertical_offset).perform()
    print("!!! NOTES FOR DEBUGGING !!! Slider changed location: " + str(slider.location))


@then('Graph is rebuilt with values "{changed_stock_value}" and "{changed_strategy_value}"')
def check_new_graph_values(context, changed_stock_value, changed_strategy_value):
    stock_bar_value = cfm.get_bar_value("#ipad-view div.stock.exmpl.column .text", context.driver)
    strategy_bar_value = cfm.get_bar_value("#ipad-view div.armor.exmpl.column .text", context.driver)
    print(
        "!!! NOTES FOR DEBUGGING !!! Changed Stock bar value: " + stock_bar_value + "; Expected: " + changed_stock_value
        + "\n!!! NOTES FOR DEBUGGING !!! Changed Strategy bar value: " + strategy_bar_value + "; Expected: " + changed_strategy_value)

    assert stock_bar_value == changed_stock_value
    assert strategy_bar_value == changed_strategy_value


@then('Screenshot is taken')
def take_screenshot(context):
    ctm.take_screenshot(context.driver)


@then('Protection widget is loaded with all the axis labels resolved into 30%, 15%, -15% and -30%')
def check_graph_axis_labels(context):
    # TODO move switch to the widget into separate commons method for this step and 'Protection widget is loaded with default values for stock -15% and strategy -5%'
    # TODO add wait to allow for parameters to be resolved

    graph_section_selector = '#protection article'

    WebDriverWait(context.driver, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, graph_section_selector)))

    active_element = context.driver.find_element_by_css_selector(graph_section_selector)
    # actions.move_to_element(active_element).perform()

    context.driver.execute_script("arguments[0].scrollIntoView();", active_element)
    context.driver.switch_to.frame(context.driver.find_element_by_css_selector("#embedded-widget"))

    element_text = context.driver.find_element_by_css_selector(
        "#ipad-view > section > div > div:nth-child(1) > div:nth-child(1)").text
    print("!!! NOTES FOR DEBUGGING !!! element_text: " + element_text)
    assert element_text == "30%"


@when('The page is scrolled and popover "{popover_number}" is clicked')  # test is unstable - see comments below
def scroll_and_click(context, popover_number):
    action = ActionChains(context.driver)

    def set_popover_id(arg):
        switcher = {
            "options #1": '#why-invest section:nth-child(2) div:nth-child(1) p:nth-child(3) > a',
            "timing risks #2": '#why-invest section:nth-child(3) a',
            "timing risks #3": '#how-it-works section:nth-child(2) > div:nth-child(2) > div:nth-child(2) > p:nth-child(1) > a'
        }
        return switcher.get(arg, "Invalid popover number")

    input_element = context.driver.find_element_by_css_selector(set_popover_id(popover_number))

    action.move_to_element(input_element).perform()

    if popover_number == "timing risks #3":  # element is hidden behind the FundFacts bar hence this action is required
        # scroll is unstable that's why 3 calls are added hoping for at least one to work
        context.driver.execute_script("window.scrollTo(0,30)")
        context.driver.execute_script("window.scrollTo(0,30)")
        context.driver.execute_script("window.scrollTo(0,30)")

    # TODO: link is not always clicked - investigate how to handle this
    action.click(input_element).perform()


@then('Popover "{popover_number}" box appears')
def check_popover_appeared(context, popover_number):
    action = ActionChains(context.driver)

    def set_popover_id(arg):
        switcher = {
            "options #1": 'tippy-1',
            "timing risks #2": 'tippy-2',
            "timing risks #3": 'tippy-3'
        }
        return switcher.get(arg, "Invalid popover number")

    WebDriverWait(context.driver, 5).until(EC.presence_of_element_located((By.ID, set_popover_id(popover_number))))

    popover_element = context.driver.find_element_by_id(set_popover_id(popover_number))
    action.move_to_element(popover_element).perform()
