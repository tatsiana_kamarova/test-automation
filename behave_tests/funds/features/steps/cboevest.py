from behave import *
from selenium.webdriver import ActionChains

from behave_tests.funds.commons import common_funds_methods as cfm
from behave_tests.commons import common_test_methods as ctm

@given('DEV cboevest.com is open')
def go_to_cboevest_site(context):
    context.driver.get("https://main-dev.cboevest.com")


@given('UAT cboevest.com is open')
def go_to_cboevest_site(context):
    context.driver.get("https://main-uat.cboevest.com")


@given('PROD cboevest.com is open')
def go_to_cboevest_site(context):
    context.driver.get("https://cboevest.com")


@when('\'Collect ongoing returns to supplement income\' is selected')
def select_vest_fit_1_2(context):
    # find_by_link_text works for Chrome, Firefox and IE but doesn't work for Edge
    # context.driver.find_element_by_link_text("Collect ongoing returns to supplement income").click()
    ctm.wait_and_click_by_css_selector(context.driver, "#i-want-to a:nth-child(2) div.grid__col-xs-11.grid__cell")

@when('\'Investing in an ETF (exchange traded fund)\' is selected')
def select_vest_fit_2_4(context):
    # find_by_link_text works for Chrome and IE but doesn't work for Firefox and Edge
    # context.driver.find_element_by_link_text("Investing in an ETF (exchange traded fund)").click()
    ctm.wait_and_click_by_css_selector(context.driver, "#ongoing-returns a:nth-child(4) div.grid__col-xs-11.grid__cell")
    # css_selector = "#ongoing-returns a:nth-child(4) div.grid__col-xs-11.grid__cell"
    # WebDriverWait(context.driver, 5).until(EC.element_to_be_clickable((By.CSS_SELECTOR, css_selector)))
    # context.driver.find_element_by_css_selector(css_selector).click()


@when('\'EXPLORE AN INCOME ETF\' button is pressed')
def press_explore_etf_button(context):
    context.driver.find_element_by_partial_link_text("EXPLORE AN INCOME ETF").click()


@then('ETF redirect popup window message appears')
def check_etf_popup_window(context):
    for handle in context.driver.window_handles:
        context.driver.switch_to.window(handle)
    etf_popup_text = context.driver.find_element_by_class_name("redirect-cboe-title").text

    print("!!! NOTES FOR DEBUGGING !!! Captured popup message for \"ETF redirect popup window message appears\" step: "
          + etf_popup_text)
    assert etf_popup_text == "You are about to leave the site and are being redirected to Cboe Vest ETFs site."


@when('PROCEED is pressed on popup window')
def press_process(context):
    context.driver.find_element_by_class_name("submit-button").click()


@when('\'Stay on this page\' is clicked on popup window')
# applicable both to Funds and ETFs popup
def click_stay_link(context):
    context.driver.find_element_by_link_text("No, stay on this page").click()


@then('ETF site is open')
def check_that_etf_site_is_opened(context):
    cfm.check_that_expected_site_is_opened(context.driver, "etfs", 10)


@then('User stays on cboevest.com site')
def check_cboevest_is_open(context):
    context.driver.implicitly_wait(10)
    cboevest_title = context.driver.title

    # Checked with Olya that it's ok to check by title if forwarding to another site worked
    print("!!! NOTES FOR DEBUGGING !!! Captured cboevest title for \"User stays on cboevest.com site\" step: "
          + cboevest_title)
    assert cboevest_title == "Cboe Vest - Protected Investments, Simplified. Option-based hedging strategies " \
                             "for stocks and ETFs. Option strategy builder. Target Outcome Investments."


@when('PRODUCTS is hovered over')
def hover_over_products(context):
    if context.driver.name in ("chrome", "internet explorer"):
        element_to_hover = context.driver.find_element_by_link_text("PRODUCTS")
    else:  # checked for MicrosoftEdge and firefox
        element_to_hover = context.driver.find_element_by_link_text("Products")

    ActionChains(context.driver).move_to_element(element_to_hover).perform()


@when("MUTUAL FUNDS is selected")
def select_mutual_funds(context):
    if context.driver.name in ("chrome", "internet explorer"):
        context.driver.find_element_by_link_text("MUTUAL FUNDS").click()
    else:  # checked for edge and firefox
        context.driver.find_element_by_link_text("Mutual Funds").click()


@when("ETFs is selected")
def select_mutual_funds(context):
    context.driver.find_element_by_link_text("ETFs").click()


@then('Mutual Funds redirect popup window message appears')
def check_mf_popup_window(context):
    for handle in context.driver.window_handles:
        context.driver.switch_to.window(handle)
    mf_popup_text = context.driver.find_element_by_class_name("redirect-cboe-title").text

    print("!!! NOTES FOR DEBUGGING !!! Captured popup message for \"Mutual Funds redirect popup window message appears\""
          " step: " + mf_popup_text)
    assert mf_popup_text == "You are about to leave the site and are being redirected to Cboe Vest Funds site."


@then('Mutual Funds page is open')
def check_that_mf_site_is_opened(context):
    cfm.check_that_expected_site_is_opened(context.driver, "mutual funds", 10)