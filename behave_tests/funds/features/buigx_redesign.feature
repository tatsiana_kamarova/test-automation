Feature: Redesign of Buffer Strategy Fund (BUIGX)

  Background:
    Given DEV Buffer Protection site is open

  @wip
  Scenario Outline: Check that all popovers can be open on BUIGX site
    When The page is scrolled and popover "<popover_number>" is clicked
    Then Popover "<popover_number>" box appears
    Examples:
      | popover_number    |
      | options #1        |
      | timing risks #2   |
      | timing risks #3   |

