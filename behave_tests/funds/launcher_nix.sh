# this script should be executed from within the behave_tests/funds folder

current_timestamp=`date -u +%Y%m%d_%H%M%S`

behave -f allure_behave.formatter:AllureFormatter -o output/$current_timestamp/jsons ./features
allure generate output/$current_timestamp/data -o output/$current_timestamp/report
