:: this script should be executed from within the behave_tests/funds folder
@echo off
for /f "delims=" %%# in ('powershell get-date -format "{yyyyMMdd_HHmmss}"') do @set current_timestamp=%%#

behave -f allure_behave.formatter:AllureFormatter -o "output/%current_timestamp%/data" ./features
allure generate "output/%current_timestamp%/data" -o "output/%current_timestamp%/report"
