from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def return_expected_page_title(name):
    if name == "protection":
        return "Cboe Vest Funds - Mutual Funds. The Buffer Protect Fund."
    elif name == "mutual funds":
        return "Cboe Vest Funds - Mutual Funds. Buffer Protect Fund. Defined Distribution Fund. " \
               "Enhanced Growth Strategy Fund. Dividend Aristocrats Target Income Fund."
    elif name == "etfs":
        return "Cboe Vest ETFs: KNG - Cboe Vest S&P 500 Dividend Aristocrats Target Income ETF"
    else:
        return "ERROR: Unknown page alias was passed"


def check_that_expected_site_is_opened(driver, page_alias, timeout):
    expected_title = return_expected_page_title(page_alias)
    timeout_error = return_timeout_error_message_for_title(expected_title, timeout)
    WebDriverWait(driver, timeout).until(EC.title_is(expected_title), timeout_error)


def return_timeout_error_message_for_title(expected_title, timeout):
    return "\n\n!!! NOTES FOR DEBUGGING !!! " \
           "Page title differs from the expected value. " \
           "Before raising a bug check if it took longer than " + str(timeout) + " seconds to load the page" \
           "\nExpected title: " + expected_title


def get_bar_value(locator, driver):
    bar_value = driver.find_element_by_css_selector(locator).text
    return bar_value