import time

from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

def take_screenshot(driver):
    path = "C:/Users/Tanya/Documents/Automation/Selenium/Screenshots/test/"
    screenshot_id = str(int(round(time.time() * 1000))) # current timestamp in milliseconds
    driver.save_screenshot(path + screenshot_id + "_screenshot.png")


def wait_and_click_by_css_selector(driver, locator):
    WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.CSS_SELECTOR, locator)))
    driver.find_element_by_css_selector(locator).click()