from selenium import webdriver

def before_feature(context, feature):
    context.driver = webdriver.Chrome()
    context.driver.set_window_position(0, 0)
    context.driver.set_window_size(950, 850)

    '''
    caps = {}
    caps['name'] = 'Behave Example'
    caps['build'] = '1.0'
    caps['browserName'] = "Chrome"  # pulls the latest version of Chrome by default
    caps['platform'] = "Windows 10"  # to specify a version, add caps['version'] = "desired version"
    caps['screen_resolution'] = '1366x768'
    caps['record_video'] = 'true'
    caps['record_network'] = 'true'
    caps['take_snapshot'] = 'true'
    '''


def before_scenario(context, scenario):
    if "skip" in scenario.effective_tags:
        scenario.skip("Marked with @skip")
        return

    #context.driver.get("https://main-dev.cboevest.com")
    #context.driver.get("https://main-uat.cboevest.com")
    #context.driver.get("https://cboevest.com")


def after_feature(context, feature):
    context.driver.quit()
    # subprocess.Popen.terminate(context.tunnel_proc)
