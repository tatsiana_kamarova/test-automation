from behave import *
from selenium.webdriver import ActionChains
from selenium.webdriver.support.wait import WebDriverWait
from behave_tests.commons import common_test_methods as ctm
from selenium.webdriver.support import expected_conditions as EC


@given('DEV Gamma page is open')
def go_to_orgs_accounts_site(context):
    context.driver.get("https://gamma.cboevest.com")


'''
@given('UAT Theta page is open')
def go_to_orgs_accounts_site(context):
    context.driver.get("https://theta.cboevest.com")


@given('PROD Demo page is open')
def go_to_orgs_accounts_site(context):
    context.driver.get("https://demo.cboevest.com")
'''

@when('Username: "{username}" and password: "{password}" are entered')
def enter_credentials(context, username, password):
    password_element = context.driver.find_element_by_name("password")
    email_element = context.driver.find_element_by_name("email")

    password_element.send_keys(password)
    email_element.send_keys(username)

    email_element.submit()


# Below just checks that user got logged in and header = Home, so if log in fails this step will fail with timeout.
# Didn't find any other clever way to implement this.
@then('User gets logged in')
def check_that_user_is_logged_in(context):
    WebDriverWait(context.driver, 15).until(EC.title_contains("Home"))
    # except TimeoutError:
    #     return "Expected page title Home was not found, possible causes: " \
    #            "\n - User didn't get logged in for some reason" \
    #            "\n - It took longer than 15 seconds to log user in"


'''
    When "<Username>" and "<password>" are entered
    Then User gets logged in
    Then User is not logged in
    And Warning message is shown asking to provide correct username and password
    '''