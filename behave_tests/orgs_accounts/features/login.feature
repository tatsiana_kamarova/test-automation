Feature: Login, authentication and logout checks on orgs accounts site

  Background:
    Given DEV Gamma page is open


  # TODO make the test to log off the first user from Examples before execution for the second user starts
  @chrome @firefox @ie @edge
  Scenario Outline: Ensure that user is logged in with correct credentials
    When Username: "<username>" and password: "<password>" are entered
    Then User gets logged in
    Examples:
      | username            | password        |
      | rdiaz0@google.ca    | test1234test    |
#      | incorrect           | incorrect       |

#  @skip
#  Scenario Outline: Ensure that user can not log in with incorrect credentials
#    When Username: "<username>" and password: "<password>" are entered
#    Then User is not logged in
#    And Warning message is shown asking to provide correct username and password
#    Examples:
#      | username | password       |
#      | admin    | wrong_pass     |
#      | advisor  | wrong_pass     |
#      | client   | wrong_pass     |