# Overview

This repository contains two packages with automated test cases created using Python/Selenium/Behave and CodeceptJS/Behave.

The second proved to be more complex and require more experience in JS so it's not actively used and left just for the reference in case someone will want to pick it up in future.
All the work in this repository will be done using Selenium and Behave, work has just started though so there's not so much to see as of now. 

Useful links which are being used when working in this repository:

1. Selenium Documentation - https://www.seleniumhq.org/docs/
2. Behave Tutorial - https://behave.readthedocs.io/en/latest/tutorial.html
3. Google and stackoverflow of course 
---

# Behave Tests

Located under ```behave_tests``` folder.

## Setup

1. PyCharm
2. Git
3. ```pip install behave```
4. ```pip install selenium```
5. Gherkin plugin in PyCharm
6. For reports generation (_under investigation_):
    - ```pip install allure-behave```
    - ```pip install pytest```
    - ```pip install junit-xml```
    - ```pip install pytest pytest-allure-adaptor```
    - ```npm install -g allure-commandline```

## Run instructions

Folders structure:

        commons
        <business_area>
        ├── features
        │   ├── steps
        │   │   └── <feature_name>.py
        │   ├── <feature_name>.feature
        │   └── environment.py
        └── TestAllFeatures<BusinessArea>.py

Where:

* Custom folders and files:
    - _commons_ - folder containing common methods used by features in different business areas (for example _take_screenshot()_)
    - _business_area_ - logical grouping of test scenarios (like Funds or OrgAccounts)
    - _TestAllFeatures<BusinessArea>.py_ - this file should be Run in PyCharm in order to execute all the scenarios from _<business_area>_ folder. _Note:_ required for Community PyCharm only, Professional PyCharm provides BDD support and tests can be executed in some different way 
* Mandatory folders and files (required by Behave in order for tests to be run. Structure of these must not be changed):
    - _features/<feature_name>.feature_ files - contain description of test scenarios in Given-When-Then format
    - _features/environment.py_ - files with some comment run parameters, specification of steps to execute before or after the feature/scenario.
    - _features/steps/<feature_name>.py_ files - contain details of scenarios implementation. *.py file must have the same name as respective *.feature file

Example:

![](behave_folders_structure.png)

There are two ways to execute tests:

1. Without HTML report: 
   - either run respective **TestAllFeatures\*.py** file (for example: _behave_tests/funds/TestAllFeaturesFunds.py_)
   - or execute ```behave``` command from the folder containing ```features``` directory (for example: _behave_tests/funds_)
2. HTML report will be created under ```output/<timestamp>/report``` folder (to view report in Chrome it needs to be launched with ```--allow-file-access-from-files```, other browsers are ok):
   - on Windows: execute ```launcher_win.bat``` from within the ```behave_tests\funds``` folder. _**Note:**_ project folder might need to be added as ```PYTHONPATH``` system variable. 
   - on Unix: ```behave_tests/funds/launcher_nix.sh``` script contains commands which should be executed to launch tests and get report generated 

## TODOs

1. Move env configuration to a separate config file:
    - Option 1: Make a change inside environment.py << this is how it works currently but disadvantage is same envrionment.py is shared by all feature files for a business area
    - Option 2: Investigate if it's possible to set up global parameters/setup for each feature.
    - Ideally option 2 should provide only alias like dev/uat/prod, admin/client, chrome/ie; address names/credentials/etc should be kept in single config file
2. Check how to implement Examples and Keywords with input parameters/parameters table
3. TBC if there are any workarounds to known disadvantages of PyCharm Community edition which doesn't support BDD:
    - No keywords suggestions when creating new scenarios
    - No mapping between keyword and its implementation 
    - It's impossible to run tests in their "classic way" (needed to create separate method that launches all tests) which complicates debugging process as well.
4. Generate test status reports (example: Serenity BDD)    
5. Ensure tests can be run on other browsers as well (default browser - Chrome)
    
       
# CodeceptJS tests

Can be found under ```codeceptjs_tests``` folder.

CodeceptJS site - https://codecept.io/

## Setup

1. PyCharm
2. Git
3. NodeJS - https://nodejs.org/en/download/
4. ```pip install selenium```
5. Install CodeceptJS - https://codecept.io/quickstart/#using-selenium-webdriver
5. Gherkin plugin in PyCharm

## Run Instructions

To launch the test execute ```codeceptjs run --steps``` command from inside the ```test-automation\codeceptjs_tests``` folder.