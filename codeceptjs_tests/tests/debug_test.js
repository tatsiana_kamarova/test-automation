
Feature('Funds');

Scenario('Ensure forwarding to Funds page works from headers menu', (I) => {
    I.amOnPage('https://main-dev.cboevest.com/en-us/');
    I.click("Mutual Funds");
    I.see("You are about to leave the site and are being redirected to Cboe Vest Funds site.");
    I.click("Proceed");
    I.seeTitleEquals("Cboe Vest Funds - Mutual Funds. Buffer Protect Fund. Defined Distribution Fund. Enhanced Growth Strategy Fund. Dividend Aristocrats Target Income Fund.");
});
